import React, { useState } from 'react';
import PropTypes from 'prop-types';
import styles from './NhlTeams.module.css';
import Menu from '../../components/Menu/index.js'
import TeamDetails from '../../components/TeamDetails/index.js'
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import { isAbsolute } from 'path';


const NhlTeams = props => {
  const [teamID, setTeamID] = useState(1);

  const updateID = function (id) {
    setTeamID(id)
  }

  return (
    <Container className={styles.root}>
      <Row className={styles.header}><Col><img alt={'hockey team'} src={"/images/headers/header.jpg"}></img></Col></Row>
      <Row noGutters>
        <Col>
          <div className={styles.main}><TeamDetails teamID={teamID} /></div>
          <div className={styles.sidenav}><Menu selectTeam={updateID} /></div>
        </Col>
      </Row>
    </Container>
  );
};

NhlTeams.defaultProps = {

};

NhlTeams.propTypes = {

};

export default NhlTeams;
