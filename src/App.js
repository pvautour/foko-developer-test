import React from 'react';
import './App.css';
import NhlTeams from './pages/NhlTeams/index.js'



function App() {
  return (
    <div className="App">
      <NhlTeams/>
    </div>
  );
}

export default App;
