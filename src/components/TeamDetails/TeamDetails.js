import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './TeamDetails.module.css';
import Table from 'react-bootstrap/Table';
import Dropdown from 'react-bootstrap/Dropdown';
import PlayerModal from '../PlayerModal/index';
import fetch from "node-fetch";




const TeamDetails = props => {
  const [roster, setRoster] = useState([]);
  const [lastSort, setLastSort] = useState('');
  const [positions, setPositions] = useState({ possiblePositions: [], positionFilter: '' });
  const [playerModalID, setPlayerModalID] = useState({ playerID: 0, show: false });

  useEffect(() => {
    fetch('https://statsapi.web.nhl.com/api/v1/teams/' + props.teamID + '/roster')
      .then(function (response) {
        if (response.status !== 200) {
          return;
        }
        response.json().then(function (data) {
          setRoster(data.roster)
        })
          .catch(function (err) {
            console.log('PlayerModal Fetch Error', err);
          });
      });
  }, [props.teamID]);

  useEffect(() => {
    let newPositions = JSON.parse(JSON.stringify(positions));
    newPositions.possiblePositions = extractPositions(roster);
    setPositions(newPositions);
  }, [positions, roster]);

  function extractPositions(roster) {
    let positions = new Set();
    roster.forEach((e) => {
      positions.add(e.position.name)
    })
    return Array.from(positions);
  }

  function cmpJerseyNumber(a, b) {
    if (parseInt(a.jerseyNumber) > parseInt(b.jerseyNumber)) return 1;
    if (parseInt(b.jerseyNumber) > parseInt(a.jerseyNumber)) return -1;
    return 0;
  }

  function cmpFullName(a, b) {
    if (a.person.fullName > b.person.fullName) return 1;
    if (b.person.fullName > a.person.fullName) return -1;
    return 0;
  }

  function getCompareFunction(colName) {
    if (colName === 'jerseyNumber') {
      return cmpJerseyNumber;
    } else if (colName === 'fullName') {
      return cmpFullName
    }
  }

  function applySortingDirection(roster, colName) {
    if (lastSort !== colName) {
      setLastSort(colName);
    } else {
      roster.reverse();
      setLastSort('');
    }
  }

  function sortBy(colName) {
    let newRoster = roster.slice();
    let cmp = getCompareFunction(colName);

    newRoster.sort(cmp);
    applySortingDirection(newRoster, colName)

    setRoster(newRoster)
  }

  function filterPositions(position = { position: '' }) {
    let newPositions = JSON.parse(JSON.stringify(positions));
    newPositions.positionFilter = position.position;
    setPositions(newPositions);
  }

  function togglePLayerModal(playerID = playerModalID.playerID) {
    setPlayerModalID({ playerID: playerID, show: !playerModalID.show });
  }

  return (
    <div className={styles.root}>
      <h1 className={styles.title}>Team Details</h1>
      <Table striped bordered hover variant="dark" size="sm" className={"text-center"}>
        <thead>
          <tr>
            <th className="align-middle" onClick={() => sortBy('jerseyNumber')}># &#8593;&#8595;</th>
            <th className="align-middle" onClick={() => sortBy('fullName')}>Name &#8593;&#8595;</th>
            <th className="align-middle">
              <Dropdown>
                <Dropdown.Toggle variant="success" id="Dropdown-basic">
                  Position
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={() => filterPositions()}>Show All</Dropdown.Item>
                  {positions.possiblePositions.map((position) =>
                    <Dropdown.Item href="#" key={position} onClick={() => { filterPositions({ position }) }}>{position}</Dropdown.Item>
                  )}
                </Dropdown.Menu>
              </Dropdown>
            </th>
          </tr>
        </thead>
        <tbody>
          {roster.map((teamMember) =>
            (!positions.positionFilter || teamMember.position.name === positions.positionFilter) &&
            <tr onClick={() => togglePLayerModal(teamMember.person.id)} key={teamMember.person.id}>
              <td>{teamMember.jerseyNumber}</td>
              <td>{teamMember.person.fullName}</td>
              <td>{teamMember.position.name}</td>
            </tr>
          )}
        </tbody>
      </Table>
      <PlayerModal playerID={playerModalID.playerID} show={playerModalID.show} togglePLayerModal={() => togglePLayerModal()}></PlayerModal>
    </div>
  );
}

TeamDetails.defaultProps = {
  
};

TeamDetails.propTypes = {

};

export default TeamDetails;
