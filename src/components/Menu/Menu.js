import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './Menu.module.css';
import Table from 'react-bootstrap/Table';
import fetch from "node-fetch";

import Collapse from 'react-bootstrap/Collapse'
import { NONAME } from 'dns';

const Menu = props => {
  const [teams, setTeams] = useState({ teams: [] });
  const [visible, setVisible] = useState(true);

  useEffect(() => {
    fetch('https://statsapi.web.nhl.com/api/v1/teams/')
      .then(function (response) {
        if (response.status !== 200) {
          return;
        }
        response.json().then(function (data) {
          setTeams(data)
        })
          .catch(function (err) {
            console.log('PlayerModal Fetch Error', err);
          });
      });
  }, []);

  function toggleNav() {
    setVisible(!visible)
  }


  return (
    <div className={styles.root}>
      <div className={styles.navbarToggle} onClick={() => toggleNav()}><img style={(visible && { width: '0px' }) || { width: '2.5rem' }} src="/images/icons/navbarToggleTransparentBG.png"></img></div>
      <div idName={"sidenav"} style={(visible && { width: '100%' }) || { width: '0px' }} className={styles.sidenav}>
        <Table striped bordered hover variant="dark" size="sm">
          <thead>
            <tr>
              <th className={'border-right-0'}>
                Teams </th>
              <th className={styles.closebtn + ' border-left-0'} onClick={() => toggleNav()}>&times;</th>
            </tr>
          </thead>
          <tbody>
            {teams.teams.map((team) =>
              <tr key={team.id} onClick={() => { toggleNav(); props.selectTeam(team.id); }}>
                <td><img className={styles.icon} alt={team.abbreviation + " Logo"} src={'/images/teamLogos/' + team.id + '.svg'}></img></td>
                <td className="align-middle">{team.name}</td>
              </tr>
            )}
          </tbody>
        </Table>
      </div>
    </div>
  )
};

Menu.defaultProps = {

};

Menu.propTypes = {

};

export default Menu;
