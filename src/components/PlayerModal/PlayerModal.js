import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import styles from './PlayerModal.module.css';
import Modal from 'react-bootstrap/Modal';
import fetch from "node-fetch";
import Table from 'react-bootstrap/Table';

const PlayerModal = props => {

  const [playerData, setPlayerData] = useState({
    "fullName": "",
    "nationality": "",
  });

  useEffect(() => {
    fetch('https://statsapi.web.nhl.com/api/v1/people/' + props.playerID)
      .then(function (response) {
        if (response.status !== 200) {
          return;
        }
        response.json().then(function (data) {
          setPlayerData(data.people[0])
        })
          .catch(function (err) {
            console.log('PlayerModal Fetch Error', err);
          });
      });
  }, [props.playerID, props.show]);

  return (
    <div className={styles.root}>
      <Modal show={props.show} onHide={props.togglePLayerModal} centered backdrop animation>
        <Modal.Header closeButton>
          <Modal.Title>{playerData.fullName}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table className={styles.table}>
            <tbody>
              <tr>
                <td className={"align-middle text-right " + styles.flag}><img src={"https://restcountries.eu/data/" + playerData.nationality.toLowerCase() + ".svg"} alt={playerData.nationality + " flag"}></img></td>
                <td className={"align-middle"}><h1>{playerData.nationality}</h1></td>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
    </div>
  );
};

PlayerModal.defaultProps = {

};

PlayerModal.propTypes = {

};

export default PlayerModal;
